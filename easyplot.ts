export enum PlotUpdate {
    Wrap,
    Extend
}

export interface IDataPoint {
    label: string,
    data: number,
}

export class PlotDictionary {
    #plots: Record<string, Plot> = {}; //Dictionary of plots, keyed by label

    readonly elements: number = 0; //Number of elements that all plots will
                                   //be created with

    readonly labels: string[]; //Initial plot label array

    readonly x_axis: number[]; //Shared X-Axis 

    readonly colors: string[] = [
        '#EF9E9E',
        '#9EEF9E',
        '#9EEFEF',
        '#EFEF9E',
    ];

    constructor(plot_labels: string[], 
        elements: number, 
        update: PlotUpdate, 
        colors?: string[]
    ){
        this.labels = plot_labels;
        this.elements = elements;
        this.x_axis = Array<number>(elements);
       
        if(colors){
            this.colors = colors;
        }

        for(let i = 0; i < elements; i++){
            this.x_axis[i] = i;
        }

        this.labels.forEach( (label, i) => {
            let p = new Plot(label, elements, update);
            if(i < this.colors.length){
                p.borderColor = this.colors[i];
                p.backgroundColor = this.colors[i];
            }else{
                p.backgroundColor = '#232305';
                p.borderColor = '#232305';
            }
            this.#plots[label] = p;
        });
    }

    /**
     * Adds a single point to a plot
     *
     * @param {IDataPoint} dataPoint - Point to add
     * @returns {void}
     */
    addDataPoint(dataPoint: IDataPoint) {
        this.#plots[dataPoint.label].addDataPoint(dataPoint);
    }

    /**
     * Adds multiple datapoints to their respective plots. 
     *
     * @param {IDataPoint[]} dataPoints - Array of datapoints
     */
    addDataPoints(dataPoints: IDataPoint[]){
        dataPoints.forEach( dp => {
            this.addDataPoint(dp);
        });
    }

    /**
     * Converts all the plots to a ChartJS friendly format.
     *
     * @returns any ChartJS JSON object
     */
    toChartJSData(): any {
        let datasets = this.labels.map( label => this.#plots[label].toChartJSDataset() );

        return {
            labels: this.x_axis,
            datasets: datasets
        }
    }
}

export class Plot {
    label: string = ''; //Label of the plot

    readonly elements: number = 0; //Number of elements in the plot array
    
    #update: PlotUpdate = PlotUpdate.Wrap; //How the plot should treat new data
    
    #data: number[] = []; //Data stored in plot to be displayed

    #currentIndex: number = 0; //Current array index

    borderColor: string; //Color of the border when drawn

    backgroundColor: string; //Color of the line when drawn

    constructor(label: string, elements: number, update: PlotUpdate){
        this.#update = update;
        this.label = label;
        this.elements = elements;
        this.backgroundColor = 'rgba(0, 0, 0, .1)';
        this.borderColor = 'rgba(0, 0, 0, .1)';

        this.#data = Array<number>(elements);
        this.#currentIndex = 0;
    }

    /**
     * Adds a point to a Dataset. 
     *
     * If the update mode is Wrap, data is added until elements is reached
     * and starts to overwrite at the begingin.
     *
     * If the update mode is Extend, data the early datapoints are discarded
     * and new data is appended.
     *
     * @param {IDataPoint} point - Datapoint to add
     * @returns void
     */
    addDataPoint(point: IDataPoint) {
        switch(this.#update){
            case PlotUpdate.Wrap:
                this.#data[this.#currentIndex] = point.data;
                this.#currentIndex += 1;
                if(this.#currentIndex >= this.elements){
                    this.#currentIndex = 0;
                }
                break;

            case PlotUpdate.Extend:
                if(this.#currentIndex < this.elements){
                    this.#data[this.#currentIndex] = point.data;
                    this.#currentIndex += 1;
                }else{
                    this.#data.shift();
                    this.#data.push(point.data);
                }
                break;
        }
    }

    /**
     * Maps the Plot to a ChartJS friendly JSON object. The object is 
     * configured according to ChartJS to have the best rendering performance.
     *
     * @returns any - Returns a ChartJS friendly JSON object
     */
    toChartJSDataset(): any {
        return {
            label: this.label,
            data: this.#data,
            showLine: true,
            spanGaps: true,
            pointRadius: 0,
            tension: 0,
            stepped: false,
            borderDash: [],
            normalized: true,
            backgroundColor: this.backgroundColor,
            borderColor: this.borderColor,
        }
    }
}
