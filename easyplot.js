"use strict";
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var _PlotDictionary_plots, _Plot_update, _Plot_data, _Plot_currentIndex;
Object.defineProperty(exports, "__esModule", { value: true });
exports.Plot = exports.PlotDictionary = exports.PlotUpdate = void 0;
var PlotUpdate;
(function (PlotUpdate) {
    PlotUpdate[PlotUpdate["Wrap"] = 0] = "Wrap";
    PlotUpdate[PlotUpdate["Extend"] = 1] = "Extend";
})(PlotUpdate = exports.PlotUpdate || (exports.PlotUpdate = {}));
class PlotDictionary {
    constructor(plot_labels, elements, update, colors) {
        _PlotDictionary_plots.set(this, {}); //Dictionary of plots, keyed by label
        this.elements = 0; //Number of elements that all plots will
        this.colors = [
            '#EF9E9E',
            '#9EEF9E',
            '#9EEFEF',
            '#EFEF9E',
        ];
        this.labels = plot_labels;
        this.elements = elements;
        this.x_axis = Array(elements);
        if (colors) {
            this.colors = colors;
        }
        for (let i = 0; i < elements; i++) {
            this.x_axis[i] = i;
        }
        this.labels.forEach((label, i) => {
            let p = new Plot(label, elements, update);
            if (i < this.colors.length) {
                p.borderColor = this.colors[i];
                p.backgroundColor = this.colors[i];
            }
            else {
                p.backgroundColor = '#232305';
                p.borderColor = '#232305';
            }
            __classPrivateFieldGet(this, _PlotDictionary_plots, "f")[label] = p;
        });
    }
    /**
     * Adds a single point to a plot
     *
     * @param {IDataPoint} dataPoint - Point to add
     * @returns {void}
     */
    addDataPoint(dataPoint) {
        __classPrivateFieldGet(this, _PlotDictionary_plots, "f")[dataPoint.label].addDataPoint(dataPoint);
    }
    /**
     * Adds multiple datapoints to their respective plots.
     *
     * @param {IDataPoint[]} dataPoints - Array of datapoints
     */
    addDataPoints(dataPoints) {
        dataPoints.forEach(dp => {
            this.addDataPoint(dp);
        });
    }
    /**
     * Converts all the plots to a ChartJS friendly format.
     *
     * @returns any ChartJS JSON object
     */
    toChartJSData() {
        let datasets = this.labels.map(label => __classPrivateFieldGet(this, _PlotDictionary_plots, "f")[label].toChartJSDataset());
        return {
            labels: this.x_axis,
            datasets: datasets
        };
    }
}
exports.PlotDictionary = PlotDictionary;
_PlotDictionary_plots = new WeakMap();
class Plot {
    constructor(label, elements, update) {
        this.label = ''; //Label of the plot
        this.elements = 0; //Number of elements in the plot array
        _Plot_update.set(this, PlotUpdate.Wrap); //How the plot should treat new data
        _Plot_data.set(this, []); //Data stored in plot to be displayed
        _Plot_currentIndex.set(this, 0); //Current array index
        __classPrivateFieldSet(this, _Plot_update, update, "f");
        this.label = label;
        this.elements = elements;
        this.backgroundColor = 'rgba(0, 0, 0, .1)';
        this.borderColor = 'rgba(0, 0, 0, .1)';
        __classPrivateFieldSet(this, _Plot_data, Array(elements), "f");
        __classPrivateFieldSet(this, _Plot_currentIndex, 0, "f");
    }
    /**
     * Adds a point to a Dataset.
     *
     * If the update mode is Wrap, data is added until elements is reached
     * and starts to overwrite at the begingin.
     *
     * If the update mode is Extend, data the early datapoints are discarded
     * and new data is appended.
     *
     * @param {IDataPoint} point - Datapoint to add
     * @returns void
     */
    addDataPoint(point) {
        switch (__classPrivateFieldGet(this, _Plot_update, "f")) {
            case PlotUpdate.Wrap:
                __classPrivateFieldGet(this, _Plot_data, "f")[__classPrivateFieldGet(this, _Plot_currentIndex, "f")] = point.data;
                __classPrivateFieldSet(this, _Plot_currentIndex, __classPrivateFieldGet(this, _Plot_currentIndex, "f") + 1, "f");
                if (__classPrivateFieldGet(this, _Plot_currentIndex, "f") >= this.elements) {
                    __classPrivateFieldSet(this, _Plot_currentIndex, 0, "f");
                }
                break;
            case PlotUpdate.Extend:
                if (__classPrivateFieldGet(this, _Plot_currentIndex, "f") < this.elements) {
                    __classPrivateFieldGet(this, _Plot_data, "f")[__classPrivateFieldGet(this, _Plot_currentIndex, "f")] = point.data;
                    __classPrivateFieldSet(this, _Plot_currentIndex, __classPrivateFieldGet(this, _Plot_currentIndex, "f") + 1, "f");
                }
                else {
                    __classPrivateFieldGet(this, _Plot_data, "f").shift();
                    __classPrivateFieldGet(this, _Plot_data, "f").push(point.data);
                }
                break;
        }
    }
    /**
     * Maps the Plot to a ChartJS friendly JSON object. The object is
     * configured according to ChartJS to have the best rendering performance.
     *
     * @returns any - Returns a ChartJS friendly JSON object
     */
    toChartJSDataset() {
        return {
            label: this.label,
            data: __classPrivateFieldGet(this, _Plot_data, "f"),
            showLine: true,
            spanGaps: true,
            pointRadius: 0,
            tension: 0,
            stepped: false,
            borderDash: [],
            normalized: true,
            backgroundColor: this.backgroundColor,
            borderColor: this.borderColor,
        };
    }
}
exports.Plot = Plot;
_Plot_update = new WeakMap(), _Plot_data = new WeakMap(), _Plot_currentIndex = new WeakMap();
