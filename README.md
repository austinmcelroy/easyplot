# Easy Plot

Helps keep track of multiple plots, with each plot having a label.

# Introduction

Easy Plot is a utility that was developed for keeping track of multiple plots
and massaging the plots to a ChartJS friendly format. When generating a ChartJS
friendly JSON object, the parameters are set for optimal rendering.

# Classes

## Plot

The Plot class requires a label, the number of elements that can be added, 
and whether the plot update format. 

### Label
Labeling is easy, just a string of how this plot should be human readable. For
example, 'ekg'.

### Elements
Number of elements that are to be buffered and displayed. This is entirely
dependent on your project and sampling rate.

### PlotUpdate
PlotUpdate can be Wrap or Extend. This is how new data is added once the number
of elements is equal to 'Elements'. In Wrap mode, the data starts over again
at the begining, think circular buffer. In Extend mode, the earliest elements
are discarded and new elements added to the end.

### addDataPoint(IDataPoint) 
Adds an IDataPoint to the graph. 

### toChartJSDataset()
Returns the Plot data in a ChartJS friendly JSON object. The JSON object has
all of the properties set for fastest rendering in ChartJS.

### Other parameters

#### borderColor 
Sets the outline color of the line of the graph (ChartJS only)

#### backgroundColor
Sets the color of the line on the graph (ChartJS only)

## PlotDictionary

The PlotDictionary class is useful if there are multiple plots that are to be 
used. The plots are stored in a dictionary according to the Plot.label. 

### addDataPoint
Adds an IDataPoint to a Plot. This uses the label of the plot and the label of
the data point to make sure data is routed correctly.

### addDataPoints
Add multiple IDataPoint objects to the plots, according to labels.

### toChartJSData()
Creates a ChartJS friendly JSON object that can be used to create a new ChartJS
object.

## Interfaces

### IDataPoint
Interface that can be added to an object. This interface requires a label, which
***MUST*** match the plot the datapoint will be added to and the data to add.

## Example
> import { Plot, PlotDictionary, PlotUpdate } from 'easyplot'
> import Chart from 'chart.js/auto' //see ChartJS for more up-to-date usage 
>
> let plotPoints = 2000;
> let labels = ['ekg', 'heart_rate'];
> 
> let plots = new PlotDictionary(labels, plotPoints, PlotUpdate.Wrap);
> //or PlotUpdate.Extend 
>
> let new_ekg: IDataPoint = {
>   label: 'ekg',
>   data: 1,
> };
>
> let new_hr: IDataPoint = {
>   label: 'heart_rate',
>   data: 60,
> };
>
> plots.addDataPoints([new_ekg, new_hr]);
> let chart = new Chart(ctx, type: 'line', data: plots.toChartJSDataset());
> //Periodically call chart.update() to redraw the graph 
